// // Easy Way
// //Store Mark's and John's mass and height
// let massMark = 78; // in kg
// let heightMark = 1.69; // in meters

// let massJohn = 92; // in kg
// let heightJohn = 1.95; // in meters

// // Calculate BMIs
// let BMIMark = massMark / (heightMark * heightMark);
// let BMIJohn = massJohn / (heightJohn * heightJohn);

// // Log BMIs to console
// console.log("Mark's BMI:", BMIMark);
// console.log("John's BMI:", BMIJohn);

// // Check if Mark has a higher BMI than John
// let markHigherBMI = BMIMark > BMIJohn;

// // Log the result
// console.log("Does Mark have a higher BMI than John?", markHigherBMI);


// =====================================
//Intermediate Modern way


// let data = {
//     Mark : {
//         Mass : 78,
//         Height: 1.69
//     },
//     John : {
//         Mass : 92,
//         Height: 1.95
//     }
// }

// BMICalc = (mass, height) => BMI = mass / (height * height)

// //  BMICalc = function (mass,height){
// //     return BMI = mass / (height * height);
// //  }
 

// let {Mark:{Mass:mMark ,Height:hMark}, John:{Mass:mJohn, Height:hJohn}} = data;

// let bMark = `using func ${BMICalc(mMark,hMark)}`;
// let bJohn = `using func ${BMICalc(mJohn,hJohn)}`;

// // if(bMark>bJohn){
// // console.log(`Mark has higher BMI and here's the result : Mark= ${bMark} and John ${bJohn}`)
// // }else{
// //     console.log(`John has higher BMI and here's the result : Mark= ${bMark} and John ${bJohn}`)
// // }

// result = bMark>bJohn ? `Mark has higher BMI and here's the result : Mark= ${bMark} and John ${bJohn}` : `John has higher BMI and here's the result : Mark= ${bMark} and John ${bJohn}`

// console.log(result);




//================== Advanced  way
// let data = {
//     Mark : {
//         Mass : 78,
//         Height: 1.69
//     },
//     John : {
//         Mass : 92,
//         Height: 1.95
//     }
// }

// // Destructering child object and passing then asargument directly
// BMICalc = ({ Mass: mass, Height: height }) => BMI = mass / (height * height);

// // Destructering main data object
// let {Mark, John} = data;

// let bMark = `${BMICalc(data.Mark)}`;
// let bJohn = `${BMICalc(data.John)}`;

// result = bMark>bJohn ? `Mark has higher BMI and here's the result : Mark= ${bMark} and John ${bJohn}` : `John has higher BMI and here's the result : Mark= ${bMark} and John ${bJohn}`

// console.log(result);



// =============== Adv
// BMI Calculator class with encapsulated logic
class BMICalculator {
    static calculate({ Mass, Height }) {
        if (!Mass || !Height || typeof Mass !== 'number' || typeof Height !== 'number' || Mass <= 0 || Height <= 0) {
            throw new Error('Invalid input data. Mass and Height must be valid positive numbers.');
        }
        const bmi = Mass / (Height * Height);
        return parseFloat(bmi.toFixed(2)); // Round BMI to two decimal places
    }
}

// Data object containing individuals' information
const data = {
    Mark: {
        Mass: 78,
        Height: 1.69
    },
    John: {
        Mass: 92,
        Height: 1.95
    }
};

// Calculate BMI for each individual using functional programming
const bmis = Object.values(data).map(person => BMICalculator.calculate(person));

// Determine the index of the person with the highest BMI
const highestBMIIndex = bmis.indexOf(Math.max(...bmis));

// Result message based on the comparison of BMIs
const result = highestBMIIndex === 0 ?
    `Mark has higher BMI (${bmis[0]}) than John (${bmis[1]})` :
    `John has higher BMI (${bmis[1]}) than Mark (${bmis[0]})`;

console.log(result);
